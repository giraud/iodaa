# IODAA

## Intro
Dire bonjour, blague pour détendre, etc, exIODAA, 

## présenter ce qu'on a fait, fait, fera

### Adi
Ex IODAA Cartereau rpz
   - Stage: Veolia R&D sujet cool mais env pas trop top, pas culture data culture ingénieur
   
   - emploi 1: DS AXA France (aka la garderie) contexte boite à papa
        - automatisation de reporting et études à la con
        - détection de faux document avec Momo

   - emploi 2: DS AXA Data & Tech Innovation (aka la garderie où on parle anglais), contexte plus internationale 
        - toutes les compétences en data représentées
        - 3 grandes échelles de maturité: LT/MT/CT
        - en charge de la partie DS de l'outil fraude Sherlock (https://m.youtube.com/watch?feature=youtu.be&v=-ARDcUmEfFs)
        équipe de dev complet, agile (Scrum)
        - computer vision: car dammage, licence plate, car model
        - test de nouveaux outils: AWS, Sage maker, ...
   - emploi 3: to be defined

### Louis-Victor
Ex IODAA promo 201X  
Cooptage par Adi toussa toussa  

@Cofely optimisation thermique batiment:
 - Modélisation du prix de l'elec
 - Simulation thermique / conso

@AXA Sinistres auto:
 - Normalisation du cout de réparation
 - `dedupy`
 - CCA (web-api-db-geojson)

@Accenture Detection fraude fiscale:
 - Impôt foncier
 - Detection piscine via segmentation d'image (deep-learning)
 - Croisement avec le plan cadastrale

## Quotidien d'un DS, mythes et légendes
Si t'aime pas coder ... t'es dans le caca pour quelques années le temps de pex en bullshit

Methodo théorique :

 0. Expression de besoin (atelier avec le metier)
 1. Identification de la cible (atelier pour valider avec le metier)
 2. Identification des data minima & data dispo (internes | externes) (metier + SI ?)
 3. Collecte (SI ?)
 4. Exploration (échange avec le metier pour comprendre WTF kesako ?)
 5. Preparation des data (typage - règles métier - feature ingineering) (go faire propre tout de suite POO powa)
 6. Modélisation (choix du modèle fonction des données / contraintes / cible (explicabilité toussa toussa))
 7. Validation (crossval > pilote)
 8. Déploiement (good luck ...)
 9. Feedback "intégration continue" (mes couilles qu'on y arrive)

Mythes:
 - Un datascientist fais uniquement de la modélisation. "Moi je ne ferais jamais un boulot de dev" ... à oublier
 - Les données sont propres
 - Les données sont facilement accessibles

Réalités
 - Les modèles c'est toujours les mêmes : GLM / RandomForest / GBM / CNN
 - Les schémas relationnels n'existent pas ou sont obsolètes.
 - Personne ne sait d'où viennent les données (reverse engineering non-stop)
 - On découvre toujours des règles à la cons qui vienne plus tard, genre au moment du pilote. D'où l'idée de bien structurer son code et de le faire clean d'entrée de jeu ("pas l'temps d'niaiser, filme mes pieds")
 - Les managers sont des mentors en datascience ...

Les sources au cours du temps:
 - Stackoverflow / Google (au début tu luttes et tu tryhard)
 - les docs (pandas - numpy - scipy - keras - torch) (tu commences à être chez toi, au chaud)
 - Le code source (vers l'infini et l'haut delà)
 - Kaggle pour pex son skill datascience
 - Tuto à foison sur le net

## Feedback sur la formation
Utiles au quotidien:
 - Python (go POO) Christine MARTIN +++ <3 <3 <3
 - Les bases de données aussi c'est important
 - On aurait du écouter les cours sur la doc

Ce qui nous a manqué:
 - `git` (nécessaire. Forcer au cours d'un projet au moins à prendre en main le bouzin, même si on comprend queud, on l'aura déjà vu)
 - bash ?
 - le web (au moins dans les grandes lignes : db | back | api | front)
 - packaging / mise en prod (c'est un metier en soi ... pas possible de voir ca à l'école)

Mon golden tip pour le premier job:
> restez pas trop longtemps si vous êtes le seul DS ... Pour apprendre faut des gens meilleurs que vous.

## Q&A

## Apéro
